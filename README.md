# :sunglasses: LIGUE developer test
Teste para seleção de desenvolvedores LIGUE

## Objetivo do teste
Olá jovem padawan!! O objetivo deste desafio é avaliar o domínio do candidato no desenvolvimento de software.
Será avaliado boas práticas de code style, organização do projeto, criação de APIs, conhecimento de frameworks e tecnologias.

Por se tratar de um teste básico não se esqueça de **MOSTRAR O MAXIMO DE CONHECIMENTO** em boas práticas.

**Aplicação dos conceitos de SOLID, Clean Architecture, Arquitetura Hexagonal/em camadas será extremamente bem vista**

## Requisitos
* API em NodeJs e Typescript (preferível <3) ou PHP
* Deve rodar em docker (bancos de dados e aplicação)
* Migrations para gerar as tabelas no banco de dados
* Testes unitários
* Caso a API seja feita em NodeJS ultilizar Express como framework

## Especificação
Monte uma base de usuários com a seguinte estrutura:

```
id: string
name: string
sex: 'M' | 'F'
age: integer
hobby: string
birthdate: date
```

Utilize o ​banco de dados​ de sua preferência para armazenar os dados que a API irá
consumir.

## API endpoints

```
GET /developers
```
Retorna todos os desenvolvedores

```
GET /developers/filter?
```
Retorna os desenvolvedores de acordo com o termo passado via querystring e
paginação

```
GET /developers/{id}
```
Retorna os dados de um desenvolvedor

```
POST /developers
```
Adiciona um novo desenvolvedor

```
PUT /developers/{id}
```
Atualiza os dados de um desenvolvedor

```
DELETE /developers/{id}
```
Apaga o registro de um desenvolvedor

## Vaga frontend
Caso deseje se candidatar para frontend deverá criar também uma aplicação que consuma os endpoints da api, UI/UX fica a critério do desenvolvedor, porém deverá ser SPA (single-page application)

## Entrega
Após finalizado o link do projeto, por e-mail, no github com explicação no README.

**Email para envio do teste e dúvidas:** matheus.gomes@ligue.net
